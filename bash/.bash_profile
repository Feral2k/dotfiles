#!/usr/bin/env bash

#
# ~/.bash_profile
#

# source .profile if it exists
if [[ -f "$HOME/.profile" ]]; then
    # shellcheck source=/dev/null
    source "$HOME/.profile"
fi

# source .bashrc if it exists and shell is interactive
if [[ -f "$HOME/.bashrc" ]] && [[ $- == *i* ]]; then
    # shellcheck source=/dev/null
    source "$HOME/.bashrc"
fi
