/**
 *
 * @licstart  The following is the entire license notice for the
 *  JavaScript code in this page.
 *
 * Copyright (C) 2023  Logan Garcia
 *
 *
 * The JavaScript code in this page is free software: you can
 * redistribute it and/or modify it under the terms of the GNU
 * General Public License (GNU GPL) as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * any later version.  The code is distributed WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU GPL for more details.
 *
 * As additional permission under GNU GPL version 3 section 7, you
 * may distribute non-source (e.g., minimized or compacted) forms of
 * that code without the copy of the GNU GPL normally required by
 * section 4, provided you include this license notice and a URL
 * through which recipients can access the Corresponding Source.
 *
 * @licend  The above is the entire license notice
 * for the JavaScript code in this page.
 *
 */

(function() {
    const DEBUG_MODE = false

    let redirectData = [
        [['youtube.com'], youtubeRedirect],
        [['reddit.com'], redditRedirect],
        [['twitter.com'], twitterRedirect],
        [['tiktok.com'], tiktokRedirect],
    ],

    replaceData = [
        [['matrix.to'], matrixToReplace],
        [['matrix:'], matrixReplace],
        [['irc:', 'ircs:'], ircReplace],
        [['tel:'], telReplace],
    ]

    const LOGS_TITLE = 'REDIRECTOR LOGS\n'
    const HTTPS = 'https://'
    const XMPP = 'xmpp:'
    const MATRIX = '@matrix.org'

    mainRedirect(redirectData)
    mainReplace(replaceData)

    function mainRedirect(cases) {
        for (let i = 0; i < cases.length; i++) {
            let currentCase = cases[i]
            let domains = currentCase[0]
            let redirectFn = currentCase[1]

            for (let j = 0; j < domains.length; j++) {
                let domain = domains[j]
                let hostHasDomain = hostHas(domain)

                if(DEBUG_MODE) {
                    console.log(LOGS_TITLE, 'DOMAIN:', domain, 'REDIRECT FN:', redirectFn, 'HOST HAS DOMAIN:', hostHasDomain)
                }

                if(hostHasDomain) {
                    return redirectFn()
                }
            }
        }
    }

    function mainReplace(cases) {
        var anchors = document.querySelectorAll('a[href]');
        Array.prototype.forEach.call(anchors, function (element, index) {
            for (let i = 0; i < cases.length; i++) {
                let currentCase = cases[i]
                let domains = currentCase[0]
                let redirectFn = currentCase[1]

                for (let j = 0; j < domains.length; j++) {
                    let domain = domains[j]

                    if(DEBUG_MODE) {
                        console.log(LOGS_TITLE, 'DOMAIN:', domain, 'REDIRECT FN:', redirectFn, 'HOST HAS DOMAIN:', hostHasDomain)
                    }

                    if(element.href.indexOf(domain) != -1) {
                        element.href = redirectFn(new URL(element.href))
                    }
                }
            }
        });
    }

    function farsideRedirect(service) {
        return redirect('farside.link/' + service)
    }

    function youtubeMusicRedirect() {
        return farsideRedirect('hyperpipe')
    }

    function quoraRedirect() {
        return farsideRedirect('quetre')
    }

    function tiktokRedirect() {
        return farsideRedirect('proxitok')
    }

    function odyseeRedirect() {
        return farsideRedirect('librarian')
    }

    function imgurRedirect() {
        return farsideRedirect('rimgo')
    }

    function mediumRedirect() {
        if(!/^\/$/.test(location.pathname)) {
            return farsideRedirect(scribe)
        } else {
            let perfObs = PerformanceObserver

            if(perfObs) {
                let obs = new PerformanceObserver((list) => {
                    let entries = list.getEntries()

                    for (let i = 0; i < entries.length; i++) {
                        let entry = entries[i]

                        if(entry.name.endsWith('graphql')) {
                            mainRedirect(location, data)
                        }
                    }
                })

                obs.observe({
                    entryTypes: ['resource']
                })
            } else {
                return farsideRedirect(scribe)
            }
        }

        function getPerformanceObserver() {
        }
    }

    function wikipediaRedirect() {
        let _host = host.split('.')
        let lang = 'en'

        if(_host.length > 2 && _host[0] !== 'www') {
            lang = _host[0]
        }

        return redirect(wikiless, '?lang=' + lang)
    }

    function twitterRedirect() {
        let _host = location.host.split('.')

        if(_host[0] === 'twitter') {
            return farsideRedirect('nitter')
        }
    }

    function redditRedirect() {
        return farsideRedirect('libreddit')
    }

    function youtubeRedirect() {
        return farsideRedirect('invidious')
    }

    function redirect(domain, _search = location.search, pathname = location.pathname) {
        if(!_search.startsWith('?')) {
            _search = '?' + _search
        }

        let redirectUrl = HTTPS + domain + pathname + _search

        if(DEBUG_MODE) {
            return console.log(LOGS_TITLE, 'URL:', redirectUrl, 'DOMAIN:', domain, 'SEARCH:', _search, 'PATHNAME:', pathname)
        } else {
            return location.replace(redirectUrl)
        }
    }

    function matrixToReplace(url) {
        url = url.href.replace('https:\/\/matrix.to\/#\/', '')
        let matrixId = url.split(':')
        let alias = matrixId[0]
        let domain = matrixId[1]

        if(alias.startsWith('#')) {
            return XMPP + alias + '#' + domain + MATRIX + '?join'
        } else if(alias.startsWith('@')) {
            alias = alias.replace('@', '')
            return XMPP + username + '_' + domain + MATRIX
        }
    }

    function matrixReplace(url) {
        url = url.pathname.split('/');
        let type = url[0]
        let matrixId = url[1].split(':')
        let alias = matrixId[0]
        let domain = matrixId[1]
        if(type === 'r') {
            return XMPP + '#' + alias + '#' + domain + MATRIX + '?join'
        } else {
            return XMPP + alias + '_' + domain + MATRIX
        }
    }

    function ircReplace(url) {
        let channel = url.pathname.replace('/', '#')
        return XMPP + channel + '%' + url.host + '@irc.cheogram.org?join'
    }

    function telReplace(url) {
        return XMPP + url.pathname + '@cheogram.com'
    }

    function hostHas(str) {
        return location.host.indexOf(str) != -1
    }
})()
