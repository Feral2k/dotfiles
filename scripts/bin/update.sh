#!/usr/bin/env bash

# Copyright 2023 Logan Garcia
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

if hash apt 2>/dev/null; then
    sudo apt update && sudo apt full-upgrade && sudo apt autoremove
fi

if hash pacman 2>/dev/null; then
    sudo pacman -Syu
fi

if hash flatpak 2>/dev/null; then
    flatpak update
fi

if [ "$WSL" = true ]; then
    cmd.exe /C choco upgrade all
fi
