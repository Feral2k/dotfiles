#!/usr/bin/env bash

# Copyright 2020 Logan Garcia
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

usage() {
    printf 'usage: %s [-h] [-p] [-y]\n\n' "$(basename "$0")"
    printf 'Bumps up the copyright year on source files\n\n'
    printf 'optional arguments:\n'
    printf ' -h\tshow this help message and exit\n'
    printf ' -p\tthe path of the files to update\n'
    printf ' -y\tthe number of years from current to replace\n' 1>&2
    exit 1
}

path=.
year='1'

# process options
while getopts 'hp:y:' opt; do
    case "${opt}" in
        h)
            # print the usage
            usage
            ;;
        p)
            path=${OPTARG}
            ;;
        y)
            year=${OPTARG}
            ;;
        *)
            # print the usage
            usage
            ;;
    esac
done

# make sure path is valid
if [[ ! -e "$path" ]]; then
    echo 'path is not valid'
    exit 1
fi

# make sure a value exists for year
if [[ -z "$year" ]]; then
    usage
fi

# get all files that match specified copyright year
previous_year=$(date +'%Y' -d "$year years ago")
current_year=$(date +'%Y')
unappended_copyright=$(grep -lr "$path" -e " $previous_year ")
appended_copyright=$(grep -lr "$path" -e "-$previous_year ")

# loop through all files that match unappended copyright
for file in ${unappended_copyright[*]}; do
    sed -e "s/$previous_year/$previous_year-$current_year/g" "$file" > "${file}.bak"
    chmod --reference="$file" "${file}.bak"
    mv -f "${file}.bak" "$file"
    echo "updated copyright on ${file##*/} to current year"
done

# loop through all files that match appended copyright
for file in ${appended_copyright[*]}; do
    sed -e "s/$previous_year/$current_year/g" "$file" > "${file}.bak"
    chmod --reference="$file" "${file}.bak"
    mv -f "${file}.bak" "$file"
    echo "updated copyright on ${file##*/} to current year"
done
