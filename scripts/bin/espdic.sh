#!/usr/bin/env bash

# Copyright 2017-2020 Logan Garcia
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# This file makes use of the ESPDIC (English-Esperanto Dictionary) project
# by Paul Denisowski, which is licensed under a Creative Commons
# Attribution 3.0 Unported License.

usage() {
    printf 'usage: %s [-h] [-s] W\n\n' "$(basename "$0")"
    printf 'Search for words within ESPDIC\n\n'
    printf 'positional arguments:\n'
    printf ' W\tthe word(s) to search for\n\n'
    printf 'optional arguments:\n'
    printf ' -h\tshow this help message and exit\n'
    printf ' -s\tthe system to use [h/x]\n' 1>&2
    exit 1
}

parse() {
    local result="$1"
    declare -A array="${2#*=}"

    for elem in "${!array[@]}"; do
        while [[ "$result" == *"$elem"* ]]; do
            replace="${array[$elem]}"
            result=${result/$elem/$replace}
        done
    done

    echo "$result"
}

# set diacritics
declare -A diacritics
diacritics=()

# process options
while getopts 'hs:' opt; do
    case "${opt}" in
        h)
            # print the usage
            usage
            ;;
        s)
            # determine which system to use
            s=${OPTARG}
            if [[ $s == 'x' ]]; then
                diacritics+=(['cx']='ĉ' ['gx']='ĝ' ['hx']='ĥ' \
                             ['jx']='ĵ' ['sx']='ŝ' ['ux']='ŭ')
            elif [[ $s == 'h' ]]; then
                diacritics+=(['ch']='ĉ' ['gh']='ĝ' ['hh']='ĥ' \
                             ['jh']='ĵ' ['sh']='ŝ' ['uh']='ŭ')
            else
                usage
            fi
            ;;
        *)
            # print the usage
            usage
            ;;
    esac
    # remove the processed option from args
    shift $((OPTIND-1))
done

# get positional argument
args="$*"

# make sure positional argument is not empty
if [[ -z "$args" ]]; then
    usage
fi

# set separators
declare -A separators
separators+=([' AND ']='.*' [' KAJ ']='.*' \
             [' OR ']='\|' [' AŬ ']='\|' \
             [' AUX ']='\|' [' AUH ']='\|')

# replace separators with grep expressions
args=$(parse "$args" "$(declare -p separators)")
# convert to lowercase
args="${args,,}"
# replace h/x-systems with diacritics
args=$(parse "$args" "$(declare -p diacritics)")

# pipe ESPDIC to grep
wget -q -O - http://www.denisowski.org/Esperanto/ESPDIC/espdic.txt \
| grep --color=always -iw "$args" | less -R
