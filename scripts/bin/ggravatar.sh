#!/usr/bin/env bash

# Copyright 2020 Logan Garcia
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

usage() {
    printf 'usage: %s [-h] [-e] [-u]\n\n' "$(basename "$0")"
    printf 'Replaces a GNOME user profile image with a Gravatar\n\n'
    printf 'optional arguments:\n'
    printf ' -h\tshow this help message and exit\n'
    printf ' -e\tthe email tied to the Gravatar\n'
    printf ' -u\tthe user that will have their profile image changed\n' 1>&2
    exit 1
}

username=$(id -u -n)
email="$username"

# process options
while getopts 'he:u:' opt; do
    case "${opt}" in
        h)
            # print the usage
            usage
            ;;
        e)
            email=${OPTARG}
            ;;
        u)
            username=${OPTARG}
            ;;
        *)
            # print the usage
            usage
            ;;
    esac
done

# make sure script is being run as root
if [[ "$EUID" -ne 0 ]]; then
  echo "Please run as root"
  exit 1
fi

# make sure username is not empty
if [[ -z "$username" ]]; then
    usage
fi

location="/var/lib/AccountsService/icons/$username"
email_hash=$(echo -n "$email" | md5sum | cut -d ' ' -f 1)
address="https://www.gravatar.com/avatar/$email_hash.png?s=96"

# retrieve the Gravatar
wget -qO "$location" "$address"
