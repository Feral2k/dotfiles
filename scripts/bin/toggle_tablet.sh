#!/usr/bin/env bash

if ! hash wlr-randr 2>/dev/null; then
    echo 'wlr-randr is not installed'
    exit 1
fi

if [ -e /tmp/toggle_tablet ]; then
 rm /tmp/toggle_tablet
 wlr-randr --output DP-1 --transform normal --pos 360,0
 wlr-randr --output DSI-1 --transform normal --pos 0,0
else # tablet mode
 touch /tmp/toggle_tablet
 wlr-randr --output DP-1 --transform 90 --pos 0,720
 wlr-randr --output DSI-1 --transform 90 --pos 196,0
fi
