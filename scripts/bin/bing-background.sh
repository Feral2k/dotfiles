#!/usr/bin/env bash

# Copyright 2023 Logan Garcia
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

new_background='/tmp/bing-background.jpg'

# exit if there is no GUI
if ! hash gsettings 2>/dev/null; then
    echo "gsettings is not installed"
    exit 1
elif ! hash curl 2>/dev/null; then
    echo "curl is not installed"
    exit 1
fi

# ping Bing server for 1 minute
for i in {1..60}; do
    ping -c1 www.bing.com &>/dev/null && break || sleep 1
done

# grab Bing image of the day
resource=$(curl -s https://www.bing.com/HPImageArchive.aspx\?format\=js\&idx\=0\&n\=1\&mkt\=en-US | sed 's/.*url":"//' | sed 's/","urlbase".*$//' | awk '{$1=$1};1' | sed 's/^/https:\/\/www.bing.com/')

# exit if image does not exist
if ! wget --quiet --spider "$resource"; then
    exit 1
fi

# set DBUS env var if it does not exist
if [ -z ${DBUS_SESSION_BUS_ADDRESS+x} ]; then
    export DBUS_SESSION_BUS_ADDRESS="unix:path=/run/user/$(id -u)/bus"
fi

# exit if gsettings does not exist
if ! hash gsettings 2>/dev/null; then
    exit 1
fi

wget --quiet -O "$new_background" "$resource"

current_background=$(gsettings get org.gnome.desktop.background picture-uri)

# exit if retrieved background is same as current background
if [[ "$current_background" == *"$new_background"* ]]; then
    exit 1
fi

gsettings set org.gnome.desktop.background picture-uri "$new_background"
changed_background=$(gsettings get org.gnome.desktop.background picture-uri)

# keep looping if background failed to change
while [[ "$changed_background" != *"$new_background"* ]]
do
    sleep 1
    gsettings set org.gnome.desktop.background picture-uri "$new_background"
    changed_background=$(gsettings get org.gnome.desktop.background picture-uri)
done

gsettings set org.gnome.desktop.background picture-options 'zoom'
