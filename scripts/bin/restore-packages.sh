#!/usr/bin/env bash

# Copyright 2023 Logan Garcia
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

system=$(arch)

if grep -iq 'microsoft' /proc/version; then
    system='work'
elif [ system == 'x86_64' ]; then
    system='laptop'
else
    system='phone'
fi

if hash apt 2>/dev/null; then
    pkglist="$HOME/.pkglists/apt/$system/.pkglist"
    if [ -f "$pkglist" ]; then
        apt install $(paste -s -d " " "$pkglist")
    fi
fi

if hash flatpak 2>/dev/null; then
    pkglist="$HOME/.pkglists/flatpak/$system/.pkglist"
    if [ -f "$pkglist" ]; then
        flatpak install flathub $(paste -s -d " " "$pkglist")
    fi
fi

if hash pacman 2>/dev/null; then
    pkglist="$HOME/.pkglists/pacman/$system/.pkglist"
    if [ -f "$pkglist" ]; then
        pacman -S --needed - < "$pkglist"
    fi
fi
