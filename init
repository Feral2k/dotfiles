#!/usr/bin/env bash

# Copyright 2018-2021 Logan Garcia
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# initialize submodules
git submodule update --init --recursive

# enable recursive subdirectory searching
shopt -s globstar

# recurse through all subfiles and directories of root folders
for item in "$PWD"/*/* "$PWD"/*/.[^.]*; do
    if [ -d "$item" ]; then
        for subitem in "$item"/**/*; do
            if [ -d "$subitem" ]; then
                continue
            fi

            make_dir="${item##*/}${subitem#"$item"}"
            make_dir=$(dirname "$HOME/$make_dir")
            src="$subitem"
            dst="$HOME/${item##*/}${subitem#"$item"}"
            echo "creating directory $make_dir if it does not exist"
            mkdir -p "$make_dir"
            #relpath=${item##*/relpath}
            echo "linking: $src -> $dst"
            ln -sfn "$src" "$dst"
        done
    else
        src="$item"
        dst="$HOME/${item##*/}"
        echo "linking: $src -> $dst"
        ln -sfn "$src" "$dst"
    fi
done

# setup flathub if flatpak is installed
if hash flatpak 2>/dev/null; then
    echo 'Adding flathub...'
    flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
fi

# reload bash
source ~/.bash_profile

# set correct permissions on directories and files
echo 'Setting permissions...'
chmod 755 ~/
mkdir -p ~/.ssh/ && chmod 700 ~/.ssh/
ls ~/.ssh/id_* 1> /dev/null 2>&1 && chmod 600 ~/.ssh/id_*
ls ~/.ssh/id_*.pub 1> /dev/null 2>&1 && chmod 640 ~/.ssh/id_*.pub
